# Digital Twin For VANET and ITS

## Introduction

The project integrates the 2D traffic simulator SUMO with the Unity game engine to create a fully interactable 3D Driving Simulation. Then, the V2X communication is realized by using NS3 and OMNETPP.

![image](https://gitlab.com/CaoDing/digital-twin-for-vanet-and-its/-/blob/main/Figure5.png)

## Software requirement

1. SUMO (Version 1.6.0)

2. Unity3D (Version 2020.3.7f1, other version may occur errorr.)

4. Omnet++ (Version 5.6.2)

## Getting started

### For the traffic simulation and 3D city environment modeling (i.e., SUMO, UNITY software):

1. Download folders ("TST5.0" is the Unity folder, "Jordan2.0" is the SUMO folder)

2. Install Unity Hub and add this folder to the path, open this folder with correct version

3. Install SUMO and open "Jordan2.0" folder. Type this command(sumo-gui -c map.sumocfg --start --remote-port 4001 --step-length 0.01) in command line under this path

4. Click "RUN" button in unity then you will see the game start

### For the vehicular network and traffic simulation (i.e., SUMO, VEINS, OMNETPP):

5. Download "safety" folder, import it and the library (e.g., inet, veins) to the Omnetpp

6. Go to mingwenv.cmd of the omnetpp, and type this command (D:/veins/sumo-launchd.py -vv -c 'D:/Program Files (x86)/Eclipse/Sumo/bin/sumo-gui.exe'). Change the sumo-gui path as your Installation path.

7. Go the "safety" -> "example" -> "TSTSUMO30" -> omnetpp.ini file, run it in the omnetpp

## Authors

Author: Cao DING

Supervisor: Dr. Ivan HO

## Acknowledgment

TraCI communication between SUMO and Unity. We modify the code from the sharing script below:
[C# port of TraCI](https://github.com/CodingConnected/CodingConnected.Traci) by @CodingConnected

The assets in unity (vehicle model, building model, trees and so on). Directly download from unity assets shop for free.

## License
Open-source project
